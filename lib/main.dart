import 'package:flutter/material.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  const ContactProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Color.fromARGB(255, 255, 134, 5),
            iconTheme: IconThemeData(color: Colors.white)),
      ),
      home: Scaffold(
        appBar: buildAppBarWidget,
        body: buildBodyWidget,
      ),
    );
  }
}

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        IconButton(
          icon: Icon(
            Icons.call,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Call"),
      ],
    );
  }
  Widget buildTextButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.textsms_outlined,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Text"),
      ],
    );
  }

  Widget buildVideoCallButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.video_camera_front,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Video"),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.email_outlined,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Email"),
      ],
    );
  }

  Widget buildDirectionsButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.directions,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Directions"),
      ],
    );
  }

  Widget buildPayButton() {
    return Column(
      children: <Widget>[

        IconButton(
          icon: Icon(
            Icons.attach_money,
            color: Colors.indigo.shade800,
          ),
          onPressed: () {},
        ),
        Text("Pay"),
      ],
    );
  }

  // "https://images8.alphacoders.com/938/thumb-1920-938042.jpg"
Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("09090989"),
    subtitle: Text("Ton"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTitle() {
  return ListTile(
    leading: Icon(null),
    title: Text("0871455483"),
    subtitle: Text("Ded"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mailListTitle() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("63160071@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget locationListTitle() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Sriracha Chonburi 20110"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

var buildAppBarWidget = AppBar(
  // backgroundColor: Color.fromARGB(255, 255, 134, 5),
  leading: Icon(
    Icons.arrow_back,
    // color: Color.fromARGB(255, 255, 255, 255),
  ),
  actions: <Widget>[
    IconButton(
        onPressed: () {
          print("Contact is starred");
        },
        icon: Icon(
          Icons.star_border,
          // color: Color.fromARGB(255, 255, 255, 255),
        ))
  ],
);

var buildBodyWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
//Height constraint at Container widget level
          height: 250,
          child: Image.network(
            "https://images8.alphacoders.com/938/thumb-1920-938042.jpg",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Meow Meow",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              buildCallButton(),
              buildTextButton(),
              buildVideoCallButton(),
              buildEmailButton(),
              buildDirectionsButton(),
              buildPayButton(),
            ],
          ),
        ),
        Divider(color: Colors.grey),
        mobilePhoneListTile(),
        otherPhoneListTitle(),
        mailListTitle(),
        locationListTitle(),
        Divider(color: Colors.grey)
      ],
    )
  ],
);





